import Head from "next/head";
import { withRouter } from "next/router";
import React, { Component } from "react";
import { connect } from "react-redux";
import { Row, Col, Card, Table, Space } from "antd";
import homeAction from "@/src/stores/home/home.action";
import CurrencyFormat from "react-currency-format";
import { Swiper, SwiperSlide, Pagination } from "swiper/react";
import { PullToRefresh } from "react-js-pull-to-refresh";
import "swiper/swiper.min.css";
import "swiper/components/pagination/pagination.min.css";

// install Swiper modules
// SwiperCore.use([Pagination]);

class Home extends Component {
  state = {
    searchComponent: false,
    activeSwiperImage: 0,
  };
  componentDidMount() {
    const { getHome, router } = this.props;
    getHome("", (status) => {
      if (status) {
        router.replace("/");
      } else {
        this.setState({ alertError: true });
      }
    });
  }

  render() {

    const { router, home } = this.props;
    const { searchComponent, activeSwiperImage } = this.state;
    return (
      <div>
        <Head>
          <title>MNC | Home</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>
        {
          home &&
          <>
            {searchComponent ? (
              <>
                <div className="tw-w-full tw-min-h-screen">

                  <div className="tw-absolute tw-top-3 tw-right-3"
                    onClick={(e) => {
                      this.setState({ searchComponent: false });
                    }}
                  >X</div>
                  <div className="tw-text-center tw-w-full tw-absolute tw-top-1/2 tw-left-1/2" style={{ transform: "translate(-50%, -50%)" }}>
                    <div>Show the QR Code below to the cashier</div>
                    <img src={home?.result.qrcode} className="tw-m-auto tw-p-5" />
                  </div>
                </div>
              </>
            ) : (
              <>
                <div className="tw-sticky tw-top-0 tw-z-40 tw-bg-white tw-w-full tw-items-center tw-shadow-md tw-block sm:tw-hidden">
                  <img className="tw-w-1/3 tw-text-right" src="assets/logo technopartner.png" />
                </div>

                <div className="tw-h-full tw-min-h-screen tw-bg-gray-100">
                  <div className="tw-p-4" style={{ backgroundImage: `url(assets/motif.png)` }}>
                    <div className="tw-p-3 tw-border tw-rounded-lg tw-bg-white">
                      <div>{home?.result.greeting}</div>
                      <div>{home?.result.name}</div>

                      <Row className="tw-mt-3">
                        <Col span={12}>
                          <Row>
                            <Col span={12} className="tw-pr-3 tw-border-r">
                              <div className="tw-p-3 tw-rounded-full tw-border"
                                onClick={(e) => {
                                  this.setState({ searchComponent: true });
                                }}
                              >
                                <img src={home?.result.qrcode} />
                              </div>
                            </Col>
                            <Col span={12} className="tw-pl-3">
                              <div>Saldo</div>
                              <div>Points</div>
                            </Col>
                          </Row>
                        </Col>
                        <Col span={12} className="tw-text-right tw-font-semibold">
                          <div>
                            <CurrencyFormat
                              value={home.result.saldo}
                              displayType={"text"}
                              thousandSeparator={true}
                              prefix={"Rp "}
                            />
                          </div>
                          <div className="tw-text-gray-400">
                            <CurrencyFormat
                              value={home.result.point}
                              displayType={"text"}
                              thousandSeparator={true}
                            />
                          </div>
                        </Col>
                      </Row>
                    </div>
                  </div>

                  <div>
                    <Swiper
                      spaceBetween={10}
                      slidesPerView={1}
                      style={{ padding: "0px", width: "100%" }}
                      pagination={{ clickable: true }}
                      onSlideChange={(swiper) => this.setState({ activeSwiperImage: swiper.activeIndex })}
                      onSwiper={(swiper) => ""}
                    >
                      {home?.result.banner.map((item, index) => (
                        <SwiperSlide key={index}>
                          <img src={item} />
                        </SwiperSlide>
                      ))}
                    </Swiper>
                    <Row className="tw-p-2 tw-bg-white">
                      <Col span={12}>
                        <Row>

                          {home?.result.banner.map((item, index) => (
                            <div key={index} style={{ width: "8px", height: "8px" }} className={`${activeSwiperImage == index ? "tw-bg-black" : "tw-bg-gray-400"} tw-m-1 tw-rounded-full`}></div>
                          ))}
                        </Row>
                      </Col>
                      <Col span={12} className="tw-text-right">
                        <div className="tw-text-gray-400">View all </div>
                      </Col>
                    </Row>
                  </div>
                </div>

                <div className="tw-sticky tw-bottom-0 tw-z-40 tw-bg-white tw-w-full tw-items-center tw-block sm:tw-hidden" style={{ boxShadow: "rgb(100 100 111 / 20%) 0px 7px 29px 0px;" }}>
                  <Row className="tw-p-2">
                    <Col span={12}>
                      <div className="tw-text-center"
                        onClick={() => {
                          router.push("/");
                        }}
                      >
                        <img src="assets/home1.png" width="12%" className="tw-m-auto" />
                        <div className="tw-text-xs">Home</div>
                      </div>
                    </Col>
                    <Col span={12}>
                      <div className="tw-text-center"
                        onClick={() => {
                          router.push("/menu");
                        }}
                      >
                        <img src="assets/menu2.png" width="12%" className="tw-m-auto" />
                        <div className="tw-text-gray-400 tw-text-xs">Menu</div>
                      </div>
                    </Col>
                  </Row>
                </div>
              </>
            )}
          </>
        }

      </div>
    );
  }
}

const mapState = (state) => ({
  home: state.home.payload,
});
const mapDispatch = {
  getHome: homeAction.browse,
};

export default withRouter(connect(mapState, mapDispatch)(Home));
