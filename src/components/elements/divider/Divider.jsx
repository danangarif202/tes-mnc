import { Component } from "react";

class Divider extends Component {
  render() {
    const { top = "32px", bottom = "32px" } = this.props;

    return (
      <>
        <div
          className="tw-w-full tw-border-b tw-border-gray-300"
          style={{
            paddingTop: top,
            marginBottom: bottom,
          }}
        ></div>
      </>
    );
  }
}

export default Divider;
