import { withRouter } from "next/router";
import { Component } from "react";
import { connect } from "react-redux";
import { Card, Space } from "antd";

class Image extends Component {
  constructor(props) {
    super(props);
    this.state = {
      image: ""
    };
  }

  componentDidMount() {
    const { id, read } = this.props
    read(id, (e) => {
      this.setState({ image: e.path });
    })
  }

  render() {
    const { image } = this.state;
    return (
      <img
        src={image}
        alt="banner-shoesmart"
        className={`${this.props.className}`}
      />
    );
  }
}



const mapState = (state) => ({});

const mapDispatch = {};

export default withRouter(connect(mapState, mapDispatch)(Image));