import { Component } from "react";
import { Row, Col, Divider, Steps } from 'antd';

class MainLayoutLogin extends Component {
  render() {
    const { router } = this.props
    const { Step } = Steps;

    return (
      <>
        <div>
          <Row>
            <Col
              span={10}
              className="tw-bg-gradient-to-t tw-from-primary-500 tw-h-screen"
            >
              <div className="tw-flex tw-justify-center">
                <div className="tw-z-10 tw-w-3/12 tw-mt-40 step">
                  <Steps direction="vertical" current={1}>
                    <Step title="Kontak Seller" />
                    <Step title="Info Domisili" />
                    <Step title="Brand Sepatu" />
                  </Steps>
                </div>
              </div>
              <div className="tw-absolute tw-top-0 tw-left-0">
                <img src="/register/20.png" />
              </div>
              <div className="tw-absolute tw-top-0 tw-left-0">
                <img src="/register/Group1.png" />
              </div>
              <div className="tw-absolute tw-bottom-0 tw-right-0">
                <img src="/register/Group.png" />
              </div>

            </Col>
            <Col span={14}>
              <main
                className="tw-px-6 tw-py-8 tw-h-screen tw-overflow-y-scroll"
              >
                {this.props.children}
              </main>
            </Col>
          </Row>
        </div>
      </>
    );
  }
}

export default MainLayoutLogin;