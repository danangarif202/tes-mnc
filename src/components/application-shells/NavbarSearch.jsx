import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Form, Image } from "antd";
import { withRouter } from "next/router";
import { Component } from "react";
import Link from "../elements/button/Link";
import MainInput from "../forms/text-field/MainInput";

class NavbarSearch extends Component {
  render() {
    const { router } = this.props;

    return (
      <>
        <div className="tw-sticky tw-top-0 tw-z-40 tw-bg-white tw-w-full tw-h-14 tw-flex tw-flex-wrap tw-items-center tw-justify-between tw-px-10 tw-align-middle tw-shadow-md">
          <Link to="/pusat-edukasi">
            <Image
              src="/logo-pusat-edu.png"
              preview={false}
              height="45px"
              width="280px"
            ></Image>
          </Link>

          <div className="tw-w-1/3">
            <Form
              onFinish={(value) => {
                console.log(value);
                router.push(`/pusat-edukasi/artikel?search=${value.search}`);
              }}
            >
              <Form.Item name="search" className="tw-mb-0">
                <MainInput
                  prefix={
                    <FontAwesomeIcon
                      icon={faSearch}
                      className="tw-mr-2 tw-text-primary-500"
                    ></FontAwesomeIcon>
                  }
                  placeholder="Cari Kata Kunci"
                ></MainInput>
              </Form.Item>
            </Form>
          </div>
        </div>
      </>
    );
  }
}

export default withRouter(NavbarSearch);
