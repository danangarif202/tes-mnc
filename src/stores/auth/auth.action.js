import authApi from "../../api/auth.api";
import authHome from "../../api/home.api";
import storageKey from "../../key/storage.key";
import storagePlugin from "../../plugins/storage.plugin";
import authType from "./auth.type";

const authAction = {
  login(cred, callback) {
    return async (dispatch) => {
      try {
        // login
        const credData = await authApi.login(cred);
        // Save credential to local storage
        storagePlugin.saveObj(storageKey.storeKey, credData.data);
        storagePlugin.saveStr(storageKey.storeToken, credData.data.access_token);

        // save to redux state
        dispatch({
          type: authType.AUTH_LOGIN,
          payload: credData,
        });

        // callback to do on component if success
        callback(true);
      } catch (error) {
        // callback to do on component if failed
        callback(false);
      }
    };
  },

  logout(callback) {
    return async (dispatch) => {
      try {
        storagePlugin.remove(storageKey.storeKey);
        storagePlugin.remove(storageKey.storeToken);
        storagePlugin.remove(storageKey.user);

        dispatch({ type: authType.AUTH_LOGOUT });
        callback(true);
      } catch (error) {
        callback(false);
      }
    };
  },
};

export default authAction;
