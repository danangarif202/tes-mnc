import { Component } from "react";
import { withRouter } from "next/router";
import { connect } from "react-redux";
import authAction from "@/src/stores/auth/auth.action";

// style and component
import { Dropdown, Menu, Col, Row, Space } from "antd";
import { faSearch, faBars, faTimes, faAngleDown, faAngleUp } from "@fortawesome/free-solid-svg-icons";
import { faBell, faHeart } from "@fortawesome/free-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import stringPlugin from "@/src/plugins/string.plugin";
import MainInput from "@/src/components/forms/text-field/MainInput";
import Button from "@/src/components/elements/button/Button";
import Link from "@/src/components/elements/button/Link";
import activeSearch from "@/src/key/active.search.key";

import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/swiper.min.css";
const { SubMenu } = Menu;

class MainNavbar extends Component {
  state = {
    bannerHeader: true,
    searchComponent: false,
    dropdownMenu: false,
    dropdownMenuMobile: false,
    listsearch: false
  };

  searchProduct(value) {
    const { browseCatalog } = this.props

    browseCatalog(param,
      () => {
        this.setState({ loading: false });
      },
      (err) => {
        this.setState({ loading: false });
      }
    );
  }

  checkIsActiveFeature() {
    const { router } = this.props;
    const { asPath } = router;

    const urlArr = asPath.split("/");
    let urlKey = urlArr[urlArr.length - 1];

    let urlKey2 = urlArr[1];

    if (activeSearch.active.includes(urlKey)) {
      return false;
    }

    if (activeSearch.active.includes(urlKey2)) {
      return false;
    }

    return true;
  }

  render() {
    const menu = [
      {
        key: "PRIA",
        title: "PRIA",
        text_color: "tw-text-black",
        route: "/gender/male",
      },
      {
        key: "WANITA",
        title: "WANITA",
        text_color: "tw-text-black",
        route: "/gender/female",
      },
      {
        key: "ANAK",
        title: "ANAK",
        text_color: "tw-text-black",
        route: "/kategori/anak",
      },
      {
        key: "NEW ARRIVALS",
        title: "NEW ARRIVALS",
        text_color: "tw-text-black",
        route: "/new-arrivals",
      },
      {
        key: "PROMO",
        title: "PROMO",
        text_color: "tw-text-black",
        route: "/promo",
      },
      {
        key: "BRAND",
        title: "BRAND",
        text_color: "tw-text-black",
        route: "/brand",
      },
      {
        key: "SALE",
        title: "SALE !",
        text_color: "tw-text-red-500",
        route: "/sale",
      },
    ];
    const { router, user } = this.props;
    const { bannerHeader, searchComponent, dropdownMenu, dropdownMenuMobile, listsearch } = this.state;

    let isActiveRoute = this.checkIsActiveFeature();

    return (
      <>
        <div className="tw-sticky tw-top-0 tw-z-40 tw-bg-white tw-w-full tw-items-center tw-shadow-md tw-hidden sm:tw-block">
          <Row className="tw-border-b tw-py-4 tw-px-10">
            <Col span={8} className="tw-flex tw-flex-wrap">
              <Space
                onClick={(e) => {
                  router.push("/");
                }}
                className="tw-cursor-pointer"
              >
                <img className="tw-w-3/4 tw-text-right" src="/logo.png" />
                <img src="/title.png" />
              </Space>
            </Col>
            <Col span={8}>
              {isActiveRoute &&
                <>
                  <MainInput
                    suffix={
                      <FontAwesomeIcon
                        icon={faSearch}
                        className="tw-text-primary-500"
                      ></FontAwesomeIcon>
                    }
                    placeholder="Cek koleksi sneaker terbaru"
                    onFocus={() => this.setState({ listsearch: true })}
                    onBlur={() => this.setState({ listsearch: false })}
                    onChange={(e) => { this.searchProduct(e.target.value) }}
                  ></MainInput>

                  {listsearch &&
                    <div className="tw-absolute tw-border tw-w-full tw-bg-white tw-rounded-lg">
                      {["Austin Sneakers dfvefdr", "Austin Sneakers werdd", "Austin Sneakers sfvgerf", "Austin Sneakers acdfdg"].map((val, index) => (
                        <div className="tw-px-3 tw-py-2 hover:tw-bg-gray-300 tw-rounded-lg tw-cursor-pointer">{val}</div>
                      ))}
                    </div>
                  }
                </>
              }
            </Col>
            <Col span={8} className="tw-flex tw-flex-wrap tw-justify-end">
              <Space>
                {(user?.name) ? (
                  <>
                    <div className="tw-mr-5 tw-font-normal tw-text-gray-500">
                      <div
                        className="tw-cursor-pointer"
                        onClick={(e) => {
                          if (dropdownMenu) {
                            this.setState({ dropdownMenu: false });
                          } else {
                            this.setState({ dropdownMenu: true });
                          }
                        }}
                      >
                        <span className="tw-mr-2">{user?.name}</span>
                        {dropdownMenu ? <FontAwesomeIcon icon={faAngleUp}></FontAwesomeIcon> : <FontAwesomeIcon icon={faAngleDown}></FontAwesomeIcon>}
                      </div>
                      {dropdownMenu &&
                        <div className="tw-absolute tw-bg-white tw-shadow-lg tw-rounded-md tw-mt-3">
                          <div className="tw-p-5">
                            <Row>
                              <Col span={6}>
                                <div
                                  className="tw-bg-gray-200 tw-text-center"
                                  style={{
                                    height: "35px",
                                    width: "35px",
                                    borderRadius: "20px",
                                    paddingTop: "6px",
                                  }}
                                >
                                  {stringPlugin.getCodeName(user?.name)}
                                </div>
                              </Col>
                              <Col span={18} className="tw-pl-3">
                                <div className="tw-truncate tw-overflow-ellipsis tw-text-black tw-text-xs tw-mb-1">{user?.name}</div>
                                <div>Lihat Akun</div>
                              </Col>
                            </Row>
                          </div>
                          <div className="tw-px-5 tw-py-3 tw-border-t"><span className="tw-cursor-pointer">Status Pesanan</span></div>
                          <div
                            className="tw-px-5 tw-py-3 tw-border-t"
                            onClick={(e) => {
                              const { router, logout } = this.props;
                              logout(() => {
                                window.location.reload();
                              });
                            }}
                          ><span className="tw-cursor-pointer">Keluar</span></div>
                        </div>
                      }
                    </div>
                  </>
                ) : (
                  <>
                    <button className="tw-h-10 tw-text-sm tw-text-primary-500 tw-px-4 tw-mx-2 tw-rounded-md tw-border tw-border-primary-500">
                      <Link to="/auth/login">Masuk</Link>
                    </button>
                    <Button className="tw-mx-2"><Link to="/auth/register">Daftar Akun</Link></Button>
                  </>
                )}
                <div className="tw-text-lg">
                  <Space className="tw-flex tw-items-center">
                    <img
                      src="/icon/favorite.png"
                      alt=""
                      className="tw-mx-2 tw-cursor-pointer"
                    />
                    <img
                      src="/icon/bell.png"
                      alt=""
                      className="tw-mx-2 tw-cursor-pointer"
                    />
                    <img
                      src="/icon/shopping-bag.png"
                      alt=""
                      className="tw-mx-2 tw-cursor-pointer"
                    />
                  </Space>
                </div>
              </Space>
            </Col>
          </Row>

          {/* NAVBAR MENU */}
          <Row className="tw-flex tw-justify-center tw-py-2 tw-px-10">
            {menu.map((item, index) => (
              <button
                className={`focus:tw-outline-none hover:tw-text-gray-500 tw-mx-3 tw-font-bold ${item.text_color}`}
                key={index}
                onClick={(e) => {
                  router.push(item.route);
                }}
              >
                {item.title}
              </button>
            ))}
          </Row>
        </div>

        <div className="tw-sticky tw-top-0 tw-z-40 tw-bg-white tw-w-full tw-items-center tw-shadow-md tw-block sm:tw-hidden">
          {searchComponent ? (
            <>
              <Row className="tw-border-b tw-py-4 tw-px-3">
                <Col span={22} className="tw-flex tw-flex-wrap">
                  <MainInput
                    suffix={
                      <FontAwesomeIcon
                        icon={faSearch}
                        className="tw-text-primary-500"
                      ></FontAwesomeIcon>
                    }
                    placeholder="Cek koleksi sneaker terbaru"
                  ></MainInput>
                </Col>
                <Col span={2} className="tw-flex tw-flex-wrap tw-justify-end">
                  <Space>
                    <FontAwesomeIcon
                      icon={faTimes}
                      className="tw-mr-2 tw-text-gray-400"
                      onClick={(e) => {
                        this.setState({ searchComponent: false });
                      }}
                    ></FontAwesomeIcon>
                  </Space>
                </Col>
              </Row>
              <Row>
                <div className="tw-min-h-screen tw-p-3">
                  Widiih kape golek opo brooo !!!
                </div>
              </Row>
            </>
          ) : (
            <Row className="tw-border-b tw-py-4 tw-px-3">
              <Col span={8} className="tw-flex tw-flex-wrap">
                <div className="tw-text-base tw-text-primary-500">
                  SHOESMART
                </div>
              </Col>
              <Col span={16} className="tw-flex tw-flex-wrap tw-justify-end">
                <Space>
                  <FontAwesomeIcon
                    icon={faSearch}
                    className={`${(user?.name) ? 'tw-mr-1' : 'tw-mr-2'} tw-text-gray-400`}
                    onClick={(e) => {
                      this.setState({ searchComponent: true });
                    }}
                  ></FontAwesomeIcon>
                  {user?.name && <img
                    src="/icon/shopping-bag.png"
                    width="80%"
                    className="tw-cursor-pointer"
                  />}
                  <FontAwesomeIcon
                    icon={faBars}
                    className="tw-mr-2 tw-text-gray-400"
                    onClick={(e) => {
                      if (dropdownMenuMobile) {
                        this.setState({ dropdownMenuMobile: false });
                      } else {
                        this.setState({ dropdownMenuMobile: true });
                      }
                    }}
                  ></FontAwesomeIcon>
                </Space>
              </Col>
              {dropdownMenuMobile &&
                <div className="tw-absolute tw-bg-white tw-shadow-lg tw-rounded-md tw-mt-7 tw-z-10 tw-right-3">
                  <div className={`tw-p-5 ${(user?.name) ? "tw-pb-3 tw-border-b" : "tw-pb-1"}`}>
                    {(user?.name) ? (
                      <Row>
                        <Col span={6}>
                          <div
                            className="tw-bg-gray-200 tw-text-center"
                            style={{
                              height: "35px",
                              width: "35px",
                              borderRadius: "20px",
                              paddingTop: "6px",
                            }}
                          >
                            {stringPlugin.getCodeName(user?.name)}
                          </div>
                        </Col>
                        <Col span={18} className="tw-pl-3">
                          <div className="tw-truncate tw-overflow-ellipsis tw-text-black tw-text-xs tw-mb-1">{user?.name}</div>
                          <div>Lihat Akun</div>
                        </Col>
                      </Row>
                    ) : (
                      <>
                        <div>
                          <button className="tw-h-10 tw-text-sm tw-text-primary-500 tw-px-4 tw-rounded-md tw-border tw-border-primary-500" style={{ width: "-webkit-fill-available" }}>
                            <Link to="/auth/login">Masuk</Link>
                          </button>
                        </div>
                        <Button className="tw-mx-0 tw-mt-2" style={{ width: "-webkit-fill-available" }}><Link to="/auth/register">Daftar Akun</Link></Button>
                      </>
                    )}
                  </div>
                  <div className="tw-px-5 tw-py-3">
                    <span className="tw-cursor-pointer">
                      <Row>
                        <img src="/icon/favorite.png" className="tw-mr-2" /> {'Favorit & Wishlist'}
                      </Row>
                    </span>
                  </div>
                  <div className="tw-px-5 tw-py-3">
                    <span className="tw-cursor-pointer">
                      <Row>
                        <img src="/icon/bell.png" className="tw-mr-2" /> Notifikasi
                      </Row>
                    </span>
                  </div>
                  <div className={`tw-px-5 tw-py-3 ${user?.name ? '' : 'tw-pb-5'}`}>
                    <span className="tw-cursor-pointer">
                      <Row>
                        <img src="/icon/shopping-bag.png" className="tw-mr-2" /> Keranjang Belanja
                      </Row>
                    </span>
                  </div>
                  {user?.name &&
                    <>
                      <div className="tw-px-5 tw-py-3 tw-border-t"><span className="tw-cursor-pointer">Status Pesanan</span></div>
                      <div
                        className="tw-px-5 tw-py-3 tw-border-t"
                        onClick={(e) => {
                          const { router, logout } = this.props;
                          logout(() => {
                            window.location.reload();
                          });
                        }}
                      ><span className="tw-cursor-pointer">Keluar</span></div>
                    </>
                  }
                </div>
              }
            </Row>
          )}
          {/* NAVBAR MENU */}
          {!searchComponent && (
            <Row>
              <Swiper
                spaceBetween={10}
                slidesPerView={"auto"}
                style={{ paddingTop: "10px", paddingBottom: "10px" }}
                onSlideChange={() => ""}
                onSwiper={(swiper) => ""}
              >
                {menu.map((item, index) => (
                  <SwiperSlide className="navbar-menu" key={index}>
                    <button
                      className={`focus:tw-outline-none hover:tw-text-gray-500 tw-mx-3 tw-font-bold ${item.text_color}`}
                      key={index}
                      onClick={(e) => {
                        router.push(item.route);
                      }}
                    >
                      {item.title}
                    </button>
                  </SwiperSlide>
                ))}
              </Swiper>
            </Row>
          )}
        </div>
      </>
    );
  }
}

const mapState = (state) => ({
  user: state.auth.user,
});
const mapDispatch = {
  logout: authAction.logout,
};

export default withRouter(connect(mapState, mapDispatch)(MainNavbar));
