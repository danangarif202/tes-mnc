import menuType from "./menu.type";

const initState = {};

function menuStore(state = initState, actions) {
  const { type, payload } = actions;
  switch (type) {
    case menuType.MENU_BROWSE:
      return {
        ...state,
        payload,
      };

    case menuType.MENU_RESET:
      return initState;

    default:
      return state;
  }
}

export default menuStore;
