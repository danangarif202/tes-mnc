import { Button } from "antd";

function ButtonTab(props) {
  const { title = "Button", textBadge = "10", isActive = false } = props;

  let newProps = new Object({ ...props });

  delete newProps.title;
  delete newProps.isActive;
  delete newProps.textBadge;

  return (
    <Button
      {...newProps}
      className={`tw-h-10 tw-text-sm tw-px-4 tw-rounded-md tw-border-none tab-button ${isActive ? "active" : ""
        } ${props.className ? props.className : ""}`}
    >
      <span>{title}</span>
      {isActive ? (
        <span className="tw-bg-primary-500 tw-rounded-full tw-py-0.5 tw-px-2 tw-text-white tw-ml-2">
          <small>{textBadge}</small>
        </span>
      ) : (
        <span className="tw-bg-gray-500 tw-rounded-full tw-py-0.5 tw-px-2 tw-text-white tw-ml-2">
          <small>{textBadge}</small>
        </span>
      )}
    </Button>
  );
}

export default ButtonTab;
