import loadingType from "./loading.type";

const loadingAction = {
  setLoading() {
    return async (dispatch) => {
      dispatch({ type: loadingType.LOADING_SET_LOADING });
    };
  },
};

export default loadingAction;
