import { faBox } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Card, Space } from "antd";
import { Component } from "react";

class CardGroup extends Component {
  render() {
    const {
      active = false,
      onClick = () => {},
      title = "Card Name",
    } = this.props;

    return (
      <>
        <Card
          className={`tw-rounded-xl tw-shadow tw-border-0 card-group tw-cursor-pointer ${
            active ? "card-group-active" : ""
          }`}
          onClick={onClick}
        >
          <Space size="middle">
            <div>
              <FontAwesomeIcon icon={faBox} size="2x"></FontAwesomeIcon>
            </div>
            <div className="tw-text-base">{title}</div>
          </Space>
        </Card>
      </>
    );
  }
}

export default CardGroup;
