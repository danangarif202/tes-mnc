function SelectField(props) {
  let className = 'py-2 px-5 focus:outline-none block w-full text-sm rounded-full'
  
  if(props.className != undefined) {
    className = `${props.className} ${className} `
  }

  const { items = [] } = props

  return (
    <>
      <select
        type="text"
        style={{
          border: '1px solid #cccccc',
        }}
        {...props}
        className={className}
      >
        <option>Select</option>
        {items.map((item) => {
          return (
            <option key={item} value={item}>{item}</option>
          )
        })}
      </select>
    </>
  );
}

export default SelectField;
