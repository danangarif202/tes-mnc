import Head from "next/head";
import { withRouter } from "next/router";
import React, { Component } from "react";
import { connect } from "react-redux";
import { Form, Input, Button, Checkbox } from 'antd';
import authAction from "@/src/stores/auth/auth.action";

class Menu extends Component {
  state = {
    alertError: false
  };
  onFinish = (values) => {
    const { login, router } = this.props;

    const payload = {
      "grant_type": "password",
      "client_secret": "0a40f69db4e5fd2f4ac65a090f31b823",
      "client_id": "e78869f77986684a",
      "username": values.username,
      "password": values.password
    }

    login(payload, (status) => {
      if (status) {
        router.replace("/");
      } else {
        this.setState({ alertError: true });
      }
    });
  };

  onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };

  render() {
    const { alertError } = this.state;
    return (
      <div>
        <Head>
          <title>MNC | Login</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>
        <>
          <div className="tw-w-full tw-min-h-screen tw-bg-gray-100">
            <img className="tw-w-9/12 tw-m-auto tw-pt-20" src="assets/logo technopartner.png" />
            <div className="tw-w-8/12 tw-m-auto tw-mt-20">
              {alertError && <div className="tw-text-center tw-mb-3 tw-text-red-500">Email atau Password Salah</div>}
              <Form
                name="basic"
                labelCol={{ span: 8 }}
                wrapperCol={{ span: 16 }}
                initialValues={{ remember: true }}
                onFinish={this.onFinish}
                onFinishFailed={this.onFinishFailed}
                autoComplete="off"
              >
                <div className="tw-text-center">Email</div>
                <Form.Item
                  name="username"
                  rules={[{ required: true, message: 'Please input your username!' }]}
                >
                  <Input
                    className="tw-rounded-lg tw-px-4 tw-py-2"
                  />
                </Form.Item>

                <div className="tw-text-center">Password</div>
                <Form.Item
                  name="password"
                  rules={[{ required: true, message: 'Please input your password!' }]}
                >
                  <Input.Password
                    className="tw-rounded-lg tw-px-4 tw-py-2"
                  />
                </Form.Item>

                <Form.Item className="tw-text-center">
                  <Button type="primary" htmlType="submit">
                    Login
                  </Button>
                </Form.Item>
              </Form>
            </div>
          </div>

        </>
      </div>
    );
  }
}

const mapState = (state) => ({});
const mapDispatch = {
  login: authAction.login,
};

export default withRouter(connect(mapState, mapDispatch)(Menu));
