const storageKey = {
  storeKey: "MNC_CLIENT_STORAGE",
  storeToken: "MNC_CLIENT_STORAGE_TOKEN",
};

export default storageKey;
