
import TextField from "./TextField";

function Input(props) {
  const { label = 'Label Input', placeholder } = props

  return (
    <>
      <div className="flex flex-wrap items-center">
        <div className="flex-shrink-0 w-1/4">
          <label>{label}</label>
        </div>
        <div className="flex-none pl-2 pr-4">
          :
        </div>
        <div className="flex-1">
          <TextField placeholder={placeholder ? placeholder : label}></TextField>
        </div>
      </div>
    </>
  );
}

export default Input;
