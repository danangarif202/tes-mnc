import loadingType from "./loading.type";

const initState = {
  loading: false,
};

function loadingStore(state = initState, actions) {
  const { type, payload } = actions;

  switch (type) {
    case loadingType.LOADING_SET_LOADING:
      return {
        ...state,
        loading: !state.loading,
      };
    default:
      return state;
  }
}

export default loadingStore;
