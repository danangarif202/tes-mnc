import alertType from "./alert.type";

const showTime = 3000;

// tutup alert dan reset message
function closeAlert(dispatch, type) {
  setTimeout(() => {
    dispatch({ type: type, payload: false, message: "" });
  }, showTime);
}

// fungsi ini hanya bisa digunakan dari **.action.js
export function showAlert(dispatch, type, message = "") {
  dispatch({ type: type, payload: true, message: message });
  closeAlert(dispatch, type);
}

const alertAction = {
  toggleAlertSuccess(message = "Berhasil") {
    return (dispatch) => {
      dispatch({
        type: alertType.ALERT_TOGGLE_SUCCESS,
        payload: true,
        message: message,
      });
      closeAlert(dispatch, alertType.ALERT_TOGGLE_SUCCESS);
    };
  },

  toggleAlertWarning(message = "Warning") {
    return (dispatch) => {
      dispatch({
        type: alertType.ALERT_TOGGLE_WARNING,
        payload: true,
        message: message,
      });
      closeAlert(dispatch, alertType.ALERT_TOGGLE_WARNING);
    };
  },

  toggleAlertDanger(message = "Error") {
    return (dispatch) => {
      dispatch({
        type: alertType.ALERT_TOGGLE_DANGER,
        payload: true,
        message: message,
      });
      closeAlert(dispatch, alertType.ALERT_TOGGLE_DANGER);
    };
  },
};

export default alertAction;
