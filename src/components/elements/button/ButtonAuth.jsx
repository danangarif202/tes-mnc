import { Button } from "antd";

function ButtonAuth(props) {
  let text = "white";
  let color = "primary";
  let border = "1";

  const { variant = "primary" } = props;

  switch (variant) {
    case "primary":
      color = "primary";
      text = "white";
      border = "1";
      break;
    case "secondary":
      color = "white";
      text = "gray";
      border = "1";
      break;
    case "success":
      color = "green";
      text = "white";
      border = "1";
      break;
    case "info":
      color = "cyan";
      text = "white";
      border = "1";
      break;
    case "warning":
      color = "amber";
      text = "white";
      border = "1";
      break;
    case "danger":
      color = "red";
      text = "white";
      border = "1";
      break;
    case "link":
      text = "gray-500";
      color = "transparent ";
      border = "0";
      break;
    default:
      color = "primary";
      text = "white";
      border = "0";
      break;
  }

  return (
    <Button
      block
      size="large"
      {...props}
      className={`${props.className} tw-my-2 tw-bg-${color}-500 tw-text-${text} tw-border-${border} tw-rounded-md focus:tw-bg-${color}-500 focus:tw-text-${text} hover:tw-bg-${color}-700 hover:tw-text-${text}`}
    >
      {props.children}
    </Button>
  );
}

export default ButtonAuth;
