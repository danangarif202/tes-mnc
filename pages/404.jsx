import { Component } from "react";
import MainLayout from "../src/components/application-shells/MainLayout";
import Link, { LinkAction } from "../src/components/elements/button/Link";

class Custom404 extends Component {
  render() {
    return (
      <MainLayout>
        <div>
          Page Not Found{" "}
          <Link to="/">
            <b>Back To Home</b>
          </Link>
        </div>
      </MainLayout>
    );
  }
}

export default Custom404;
