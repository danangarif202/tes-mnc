
import SelectField from "./SelectField";

function Select(props) {
  const { label = 'Label Input' } = props

  return (
    <>
      <div className="flex flex-wrap items-center">
        <div className="flex-shrink-0 w-1/4">
          <label>{label}</label>
        </div>
        <div className="flex-none pl-2 pr-4">
          :
        </div>
        <div className="flex-1">
          <SelectField items={['Aku', 'Kamu', 'Dan Dia']}></SelectField>
        </div>
      </div>
    </>
  );
}

export default Select;
