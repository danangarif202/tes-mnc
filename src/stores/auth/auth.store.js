import storageKey from "../../key/storage.key";
import storagePlugin from "../../plugins/storage.plugin";
import authType from "./auth.type";

const initState = {
  isUser: storagePlugin.isExist(storageKey.storeKey),
  token: storagePlugin.getStr(storageKey.storeToken),
  credential: storagePlugin.getObj(storageKey.storeKey),
  user: storagePlugin.getObj(storageKey.user),
};

function authStore(state = initState, actions) {
  const { type, payload, user } = actions;

  switch (type) {
    case authType.AUTH_LOGIN:
      return {
        ...state,
        isUser: storagePlugin.isExist(storageKey.storeKey),
        token: payload.data.token,
        credential: payload.data,
        user: user,
      };

    case authType.AUTH_LOGOUT:
      return {
        ...state,
        isUser: false,
        token: null,
        credential: null,
      };

    default:
      return state;
  }
}

export default authStore;
