import { Input } from "antd";

function MainInput(props) {
  return (
    <>
      <Input
        placeholder="Isi Field"
        {...props}
        className={`tw-rounded-lg tw-px-4 tw-py-2 ${props.className}`}
      />
    </>
  );
}

export default MainInput;
