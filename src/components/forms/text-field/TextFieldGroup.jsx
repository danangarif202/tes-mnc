function TextFieldGroup(props) {
  return (
    <>
      <div
        className="flex rounded-full px-3 align-middle items-center"
        style={{
          border: "1px solid #cccccc",
        }}
      >
        <div className="flex-none self-center my-auto text-sm">prefix</div>
        <input className="flex-1 py-2 focus:outline-none px-3 text-sm"></input>
        <div className="flex-none text-sm">suffix</div>
      </div>
    </>
  );
}

export default TextFieldGroup;
