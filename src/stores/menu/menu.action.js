import menuType from "./menu.type";
import storePlugin from "../../plugins/store.plugin";
import menuApi from "../../api/menu.api";

const menuAction = {
  browse(params) {
    return async (dispatch) => {
      storePlugin.action(async () => {
        const data = await menuApi.browse(params);
        dispatch({ type: menuType.MENU_BROWSE, payload: data });
      }, dispatch);
    };
  },

  reset() {
    return (dispatch) => {
      dispatch({ type: menuType.MENU_RESET });
    };
  },
};

export default menuAction;
