import { Col, Space, Row } from "antd";
import { Component } from "react";

class Footer extends Component {
  render() {
    const menuBantuan = [
      "Cara Pemesanan Sepatu",
      "Layanan Pengiriman",
      "Metode Pembayaran",
      "Ketentuan retur",
      "Kontak Kami",
      "FAQ",
      "Bergabung Menjadi Seller",
    ];

    const menuInfo = [
      "Ketentuan Pengguna",
      "Kebijakan Privasi",
      "Koleksi Brand Sepatu",
      "Tentang Shoesmart",
      "Shoesmart Crew",
      "Blog",
      "Karir",
    ];

    return (
      <>
        <div className="tw-bg-white">
          <Row className="tw-p-5 tw-border-b tw-border-t">
            <Col xs={12} md={8} lg={8}>
              <Row className="tw-pr-1 sm:tw-pr-0">
                <Col xs={24} md={12} lg={12}>
                  <div className="tw-font-semibold tw-text-primary-500 tw-my-3">
                    Bantuan
                  </div>
                  {menuBantuan.map((item, index) => (
                    <div
                      className={`tw-cursor-pointer tw-text-gray-500 tw-my-2`}
                      key={index}
                    >
                      {item}
                    </div>
                  ))}
                </Col>
                <Col xs={24} md={12} lg={12}>
                  <div className="tw-font-semibold tw-text-primary-500 tw-my-3">
                    Info Shoesmart
                  </div>
                  {menuInfo.map((item, index) => (
                    <div
                      className={`tw-cursor-pointer tw-text-gray-500 tw-my-2`}
                      key={index}
                    >
                      {item}
                    </div>
                  ))}
                </Col>
              </Row>
            </Col>
            <Col xs={12} md={8} lg={12}>
              <Row className="tw-pl-1 sm:tw-pr-0">
                <Col xs={24} md={12} lg={8}>
                  <div className="tw-font-semibold tw-text-primary-500 tw-my-3">
                    Metode Pembayaran
                  </div>
                  <Row>
                    {[28, 29, 30, 31, 32, 33, 34, 36].map((item, index) => (
                      <Col key={index} span={9}>
                        <img
                          src={`/icon/metode-pembayaran/image ${item}.png`}
                          className="tw-my-1"
                        />
                      </Col>
                    ))}
                  </Row>
                </Col>
                <Col xs={24} md={12} lg={8}>
                  <div className="tw-font-semibold tw-text-primary-500 tw-my-3">
                    Layanan Pengiriman
                  </div>
                  <Row>
                    {[37, 38, 39, 40, 41].map((item, index) => (
                      <Col key={index} span={9}>
                        <img
                          src={`/icon/layanan-pengiriman/image ${item}.png`}
                          className="tw-my-1"
                        />
                      </Col>
                    ))}
                  </Row>
                </Col>
                <Col xs={24} md={12} lg={8}>
                  <div className="tw-font-semibold tw-text-primary-500 tw-my-3">
                    Ikuti Kami
                  </div>
                  <Space>
                    {[20, 16, 17].map((item, index) => (
                      <img
                        key={index}
                        src={`/icon/ikuti-kami/image ${item}.png`}
                      />
                    ))}
                  </Space>
                </Col>
              </Row>
            </Col>
            <Col
              xs={24}
              md={8}
              lg={4}
              className="tw-text-center sm:tw-text-left"
            >
              <div className="tw-font-semibold tw-text-primary-500 tw-my-3">
                Aplikasi Mobile
              </div>
              <div className="tw-mt-2 tw-text-xs">
                Download aplikasi Shoesmart yang tersedia di android dan iOS
              </div>
              <Space className="tw-mt-2">
                <img src="/icon/aplikasi-mobile/appstore.png" />
                <img src="/icon/aplikasi-mobile/playstore.png" />
              </Space>
            </Col>
          </Row>
          <Col
            xs={24}
            md={24}
            lg={24}
            className="tw-text-center tw-mt-5 tw-p-5"
          >
            <img src="/logo.png" className="tw-mx-auto tw-mb-5" />
            <div>
              @ 2019- 2021. PT Sepatu Teknologi Indonesia.All Rights reserved
            </div>
          </Col>
        </div>
      </>
    );
  }
}

export default Footer;
