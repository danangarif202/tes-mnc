import { Component } from "react";
import Footer from "./Footer";
import NavbarSearch from "./NavbarSearch";

class NoSidebarLayout extends Component {
  render() {
    const { children } = this.props;
    return (
      <>
        {/* Navbar */}
        <NavbarSearch></NavbarSearch>

        <main>{children}</main>

        {/* Footer */}
        <Footer></Footer>
      </>
    );
  }
}

export default NoSidebarLayout;
