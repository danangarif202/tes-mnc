import { Component } from "react";
import { Row, Col, Divider } from "antd";

class MainLayoutLogin extends Component {
  render() {
    return (
      <>
        <div>
          <Row>
            <Col
              span={10}
              className="tw-bg-gray-100 tw-h-screen tw-hidden lg:tw-block"
              style={{
                backgroundImage:
                  "url(https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MXx8aHVtYW58ZW58MHx8MHw%3D&ixlib=rb-1.2.1&w=800)",
                backgroundSize: "auto",
                backgroundRepeat: "no-repeat",
                backgroundPosition: "center",
              }}
            ></Col>
            <Col xs={24} lg={14}>
              <main className="tw-px-6 tw-py-8 tw-h-screen tw-overflow-y-scroll">
                {this.props.children}
              </main>
            </Col>
          </Row>
        </div>
      </>
    );
  }
}

export default MainLayoutLogin;
