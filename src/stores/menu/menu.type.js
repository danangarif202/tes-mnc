const menuType = {
  MENU_BROWSE: "MENU_BROWSE",
  MENU_RESET: "MENU_RESET"
};

export default menuType;
