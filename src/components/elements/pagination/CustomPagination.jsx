import {
  faChevronLeft,
  faChevronRight,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Component } from "react";

class CustomPagination extends Component {
  render() {
    const {
      pagination = {
        current_page: 1,
        last_page: 5,
        total: 50,
        per_page: 10,
      },
      current = 1,
      onChange = () => {},
    } = this.props;

    const {
      current_page = 1,
      last_page = 5,
      total = 50,
      per_page = 10,
    } = pagination;

    let pages = [];

    for (let index = 0; index < last_page; index++) {
      pages.push(index + 1);
    }

    return (
      <>
        <div className="tw-flex tw-flex-wrap tw-justify-center">
          <div
            style={{
              width: "30px",
              height: "30px",
              lineHeight: "30px",
              textAlign: "center",
              margin: "0 5px",
              cursor: "pointer",
            }}
            onClick={(e) => {
              const page = current - 1;
              if (page > 0) {
                onChange(page);
              }
            }}
          >
            <FontAwesomeIcon icon={faChevronLeft}></FontAwesomeIcon>
          </div>
          {pages.map((page, index) => {
            return (
              <div
                key={index}
                style={{
                  width: "30px",
                  height: "30px",
                  lineHeight: "30px",
                  textAlign: "center",
                  margin: "0 5px",
                  cursor: "pointer",
                }}
                onClick={(e) => {
                  onChange(page);
                }}
                className={
                  current === page
                    ? "tw-shadow-md tw-rounded-full tw-text-primary-500"
                    : ""
                }
              >
                {page}
              </div>
            );
          })}
          <div
            style={{
              width: "30px",
              height: "30px",
              lineHeight: "30px",
              textAlign: "center",
              margin: "0 5px",
              cursor: "pointer",
            }}
            onClick={(e) => {
              const page = current + 1;
              if (page <= last_page) {
                onChange(page);
              }
            }}
          >
            <FontAwesomeIcon icon={faChevronRight}></FontAwesomeIcon>
          </div>
        </div>
      </>
    );
  }
}

export default CustomPagination;
