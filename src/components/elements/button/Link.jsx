import { withRouter } from "next/router";
import { Component } from "react";

class Link extends Component {
  render() {
    const { to = "/", router, children } = this.props;
    return (
      <a
        className=""
        href={to == "" ? "/" : to}
        onClick={(e) => {
          e.preventDefault();
          router.push(to);
        }}
      >
        {children}
      </a>
    );
  }
}

class LinkActionComponent extends Component {
  render() {
    const { children, onClick } = this.props;
    return (
      <a className="" href="#" onClick={onClick}>
        {children}
      </a>
    );
  }
}

export const LinkAction = withRouter(LinkActionComponent);

export default withRouter(Link);
