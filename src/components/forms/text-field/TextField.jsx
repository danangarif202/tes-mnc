function TextField(props) {
  let className = 'py-2 px-5 focus:outline-none block w-full text-sm rounded-full'
  
  if(props.className != undefined) {
    className = `${props.className} ${className} `
  }

  return (
    <>
      <input
        type="text"
        style={{
          border: '1px solid #cccccc',
        }}
        {...props}
        className={className}
      />
    </>
  );
}

export default TextField;
