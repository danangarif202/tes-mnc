import { Component } from "react";
import MainNavbar from "./MainNavbar";
import Footer from "./Footer";

class MainLayout extends Component {
  render() {
    return (
      <>
        <div className="main-container" style={{ backgroundColor: "white" }}>
          <MainNavbar></MainNavbar>
          <main className="tw-mb-8 tw-h-full tw-min-h-screen">
            {this.props.children}
          </main>
        </div>
      </>
    );
  }
}

export default MainLayout;
