import alertType from "./alert.type";

const initState = {
  showAlertSuccess: false,
  showAlertWarning: false,
  showAlertDanger: false,
  message: "",
};

function alertStore(state = initState, actions) {
  const { type, payload, message } = actions;

  switch (type) {
    case alertType.ALERT_TOGGLE_SUCCESS:
      return {
        ...state,
        showAlertSuccess: payload,
        message: message,
      };

    case alertType.ALERT_TOGGLE_WARNING:
      return {
        ...state,
        showAlertWarning: payload,
        message: message,
      };

    case alertType.ALERT_TOGGLE_DANGER:
      return {
        ...state,
        showAlertDanger: payload,
        message: message,
      };

    default:
      return state;
  }
}

export default alertStore;
