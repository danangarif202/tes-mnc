import "antd/dist/antd.css";
import "tailwindcss/tailwind.css";
import "@fortawesome/fontawesome-svg-core/styles.css";
import "@/src/styles/globals.css";
import { config } from "@fortawesome/fontawesome-svg-core";
import { Provider } from "react-redux";
import configureAppStore from "../src/stores/store";
import storageKey from "../src/key/storage.key";
import storagePlugin from "../src/plugins/storage.plugin";
import { Component } from "react";
import { withRouter } from "next/router";
import loadingType from "../src/stores/loading/loading.type";

config.autoAddCss = false;
const store = configureAppStore();

class MyApp extends Component {
  componentDidMount() {
    const { router } = this.props;

    this.appGuard();

    router.events.on("routeChangeStart", this.handleRouteChange);
    router.events.on("routeChangeComplete", this.handleRouteChangeComplete);
  }

  componentWillUnmount() {
    const { router } = this.props;
    router.events.off("routeChangeStart", this.handleRouteChange);
    router.events.off("routeChangeComplete", this.handleRouteChangeComplete);
  }

  appGuard() {
    const { router } = this.props;
    const isUser = storagePlugin.isExist(storageKey.storeKey);
    const url = router.route;

    if (isUser) {
      if (url.includes("/login")) {
        router.replace("/");
      }
    }

    if (!isUser) {
      if (!url.includes("/login")) {
        router.replace("/login");
      }
    }
  }

  handleRouteChangeComplete = (url) => {
    const { router } = this.props;
    const isUser = storagePlugin.isExist(storageKey.storeKey);

    if (!isUser) {
      if (!url.includes("/login")) {
        router.replace("/login");
      }
    }

    store.dispatch(this.setLoading());
  };

  handleRouteChange = (url) => {
    store.dispatch(this.setLoading());

    const { router } = this.props;
    const isUser = storagePlugin.isExist(storageKey.storeKey);

    if (!isUser) {
      if (!url.includes("/login")) {
        router.replace("/login");
      }
    }
  };

  setLoading() {
    return {
      type: loadingType.LOADING_SET_LOADING,
    };
  }

  render() {
    const { Component, pageProps } = this.props;
    return (
      <>
        <Provider store={store}>
          <Component {...pageProps} />
        </Provider>
      </>
    );
  }
}

export default withRouter(MyApp);
