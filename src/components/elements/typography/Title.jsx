import { Component } from "react";

class Title extends Component {
  render() {
    const {title = "Title"} = this.props
    return (
      <div>
        <h1 className="tw-font-bold tw-text-2xl">{title}</h1>
      </div>
    )
  }
}

export default Title