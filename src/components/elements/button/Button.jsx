import { Button } from "antd";

function MainButton(props) {
  let type = "primary";
  let background = "";
  let color = "tw-text-white";

  const { variant = "primary", bg = "solid", dark = false } = props;

  switch (variant) {
    case "link":
      type = "text";
      break;
    case "outline":
      type = "";
      break;
    default:
      type = variant;
      break;
  }

  if (!dark) {
    color = "tw-text-gray-500";
  }

  if (type == "primary") {
    color = "tw-text-white";
  }

  if (bg === "gradient") {
    background = "button-gradient";
  }

  return (
    <Button
      {...props}
      className={`tw-h-10 tw-text-sm tw-px-4 tw-rounded-md ${
        variant == "outline" ? "tw-bg-transparent" : ""
      } ${props.className ? props.className : ""} ${background} ${color}`}
      type={type}
    >
      {props.children}
    </Button>
  );
}

export function MainOutlineButton(props) {
  let type = "primary";

  const { variant = "primary" } = props;

  switch (variant) {
    case "link":
      type = "text";
      break;
    case "outline":
      type = "";
      break;
    default:
      type = "primary";
      break;
  }

  return (
    <Button
      {...props}
      className={`tw-h-10 tw-text-sm tw-px-4 tw-rounded-md`}
      type={type}
    >
      {props.children}
    </Button>
  );
}

export default MainButton;
