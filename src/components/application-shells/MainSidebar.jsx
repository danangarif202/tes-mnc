import { faHome } from "@fortawesome/free-solid-svg-icons";
import { Menu } from "antd";
import SubMenu from "antd/lib/menu/SubMenu";
import { withRouter } from "next/router";
import { Component } from "react";

const menus = [
  {
    key: "/",
    title: "Dashboard",
    route: "/",
    icon: faHome,
  },
  {
    key: "pesanan",
    title: "Pesanan",
    icon: faHome,
    route: "/pesanan",
    childrens: [
      {
        key: "pesanan/pesanan-baru",
        title: "Pesanan Baru",
        icon: faHome,
        route: "/pesanan/pesanan-baru",
      },
      {
        key: "pesanan/siap-dikirim",
        title: "Siap Dikirim",
        icon: faHome,
        route: "/pesanan/siap-dikirim",
      },
      {
        key: "pesanan/dalam-pengiriman",
        title: "Dalam Pengiriman",
        icon: faHome,
        route: "/pesanan/dalam-pengiriman",
      },
      {
        key: "pesanan/selesai",
        title: "Selesai",
        icon: faHome,
        route: "/pesanan/selesai",
      },
    ],
  },
  {
    key: "produk",
    title: "Produk",
    icon: faHome,
    childrens: [
      {
        key: "produk/tambah-produk",
        title: "Tambah Produk",
        childrens: [
          {
            key: "produk/tambah/satuan",
            title: "Tambah Satuan",
            route: "/produk/tambah/satuan",
          },
          {
            key: "produk/tambah/massal",
            title: "Tambah Massal",
            route: "/produk/tambah/massal",
          },
        ],
      },
      {
        key: "produk/aktif",
        title: "Produk Aktif",
        route: "/produk/aktif",
        icon: faHome,
      },
      {
        key: "produk/non-aktif",
        title: "Produk Non Aktif",
        route: "/produk/non-aktif",
        icon: faHome,
      },
      {
        key: "produk/dalam-moderasi",
        title: "Dalam Moderasi",
        route: "/produk/dalam-moderasi",
        icon: faHome,
      },
    ],
  },
  {
    key: "pengaturan",
    title: "Pengaturan",
    icon: faHome,
    childrens: [
      {
        key: "pengaturan/pengiriman",
        title: "Pengiriman",
        route: "/pengaturan/pengiriman",
      },
      {
        key: "pengaturan/ubah-kata-sandi",
        title: "Ubah Kata Sandi",
        route: "/pengaturan/ubah-kata-sandi",
      },
    ],
  },
  {
    key: "pusat-edukasi",
    title: "Pusat Edukasi",
    icon: faHome,
    route: "/pusat-edukasi",
  },
];

class MainSidebar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedKey: [],
      openKey: [],
    };
  }

  componentDidMount() {
    const { router } = this.props;
    const key = router.asPath.split("/");
    const idxMenu = this.findOpenedMenuIdx(key);

    this.setActive(idxMenu, key);
  }

  componentDidUpdate(nextProps) {
    const { router } = this.props;
    const key = router.asPath.split("/");
    const idxMenu = this.findOpenedMenuIdx(key);

    if (router.asPath != nextProps.router.asPath) {
      this.setActive(idxMenu, key);
    }
  }

  findOpenedMenuIdx(key) {
    const idxMenu = menus.findIndex((menu) => {
      if (menu.key === key[1]) {
        return true;
      } else {
        if (menu.childrens != undefined) {
          const idxChild = menu.childrens.findIndex(
            (child) => child.key == key[1]
          );

          if (idxChild != -1) {
            return true;
          }
        }
        return false;
      }
    });

    return idxMenu;
  }

  setActive(idxMenu, key) {
    if (idxMenu == -1) {
      this.setState({
        selectedKey: "/",
        openKey: [],
      });
    } else {
      this.setState({
        selectedKey: [`${key[1]}${key[2] ? `/${key[2]}` : ""}`],
        openKey: [menus[idxMenu].key],
      });
    }
  }

  render() {
    const { selectedKey, openKey } = this.state;
    const { router } = this.props;

    return (
      <div
        className="tw-fixed tw-bg-white tw-shadow-md tw-overflow-y-scroll"
        style={{ height: "100vh", width: "288px", top: "0", left: "0" }}
      >
        <div className="tw-pt-6 tw-pb-10 tw-px-6">
          <img src="/logo.svg" className="tw-mx-auto" alt="" height="50" />
        </div>

        <Menu
          openKeys={openKey}
          selectedKeys={selectedKey}
          mode="inline"
          onClick={(e) => {
            this.setState({ selectedKey: e.key });
          }}
          onOpenChange={(e) => {
            this.setState({ openKey: e });
          }}
        >
          {menus.map((menu) => {
            if (menu.childrens === undefined) {
              return (
                <Menu.Item
                  key={menu.key}
                  onClick={() => {
                    this.props.router.push(menu.route);
                  }}
                >
                  {menu.title}
                </Menu.Item>
              );
            } else {
              return (
                <SubMenu
                  key={menu.key}
                  title={menu.title}
                  onTitleClick={(e) => {
                    if (menu.route !== undefined) {
                      router.push(menu.route);
                    }
                  }}
                >
                  {menu.childrens.map((child) => {
                    if (child.childrens === undefined) {
                      return (
                        <Menu.Item
                          key={child.key}
                          onClick={() => {
                            this.props.router.push(child.route);
                          }}
                        >
                          {child.title}
                        </Menu.Item>
                      );
                    } else {
                      return (
                        <SubMenu key={child.key} title={child.title}>
                          {child.childrens.map((son) => {
                            return (
                              <Menu.Item
                                key={son.key}
                                onClick={() => {
                                  this.props.router.push(son.route);
                                }}
                              >
                                {son.title}
                              </Menu.Item>
                            );
                          })}
                        </SubMenu>
                      );
                    }
                  })}
                </SubMenu>
              );
            }
          })}
        </Menu>
      </div>
    );
  }
}

export default withRouter(MainSidebar);
