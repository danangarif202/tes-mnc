import Head from "next/head";
import { withRouter } from "next/router";
import React, { Component } from "react";
import { connect } from "react-redux";
import { Row, Col, Card, Table, Space, Tabs } from "antd";
import menuAction from "@/src/stores/menu/menu.action";
import CurrencyFormat from "react-currency-format";

import { Swiper, SwiperSlide, Pagination } from "swiper/react";
import "swiper/swiper.min.css";
import "swiper/components/pagination/pagination.min.css";

// install Swiper modules
// SwiperCore.use([Pagination]);
const { TabPane } = Tabs;

class Menu extends Component {
  state = {
    searchComponent: false,
    activeMenu: "Seasonal menu"
  };
  componentDidMount() {
    const { getMenu, router } = this.props;
    getMenu({ show_all: 1 });
  }

  changeView(value) {
    var elmnt = document.getElementById(value);
    var headerOffset = 300;
    var elementPosition = elmnt.getBoundingClientRect().top;
    var offsetPosition = elementPosition - headerOffset;

    elmnt.scrollIntoView();
  }

  render() {

    const { router, menu } = this.props;
    const { searchComponent, activeMenu } = this.state;

    return (
      <div>
        <Head>
          <title>MNC | Menu</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>

        <>
          <div className="tw-sticky tw-top-0 tw-z-40 tw-bg-white tw-w-full tw-items-center tw-shadow-md tw-block sm:tw-hidden">
            <div className="tw-font-semibold tw-text-center tw-py-3 tw-px-2">MENU</div>
            <div>
              <Row>
                <Swiper
                  spaceBetween={10}
                  slidesPerView={"auto"}
                  style={{ paddingTop: "0px", paddingBottom: "0px" }}
                  onSlideChange={() => ""}
                  onSwiper={(swiper) => ""}
                >
                  {menu?.result.categories.map((item, index) => (
                    <SwiperSlide className="navbar-menu" key={`m` + index}>
                      <button
                        className={`focus:tw-outline-none tw-pb-2 tw-mx-3 tw-font-semibold ${item.category_name == activeMenu ? "tw-text-black tw-border-b-2 tw-border-black" : "tw-text-gray-400"}`}
                        key={index}
                        onClick={(e) => {
                          this.setState({ activeMenu: item.category_name })
                          this.changeView(item.category_name)
                        }}
                      >
                        {item.category_name}
                      </button>
                    </SwiperSlide>
                  ))}
                </Swiper>
              </Row>
            </div>
          </div>

          <div className="tw-h-full tw-min-h-screen tw-bg-gray-100">
            {menu?.result.categories.map((item, index) => (
              <>
                <div id={item.category_name} className="tw-font-semibold tw-py-2 tw-px-3" key={`x` + index}>{item.category_name}</div>
                {item.menu.map((item, index) => (
                  <>
                    <Row className="tw-border tw-p-2 tw-bg-white" key={`j` + index}>
                      <Col span={4}>
                        <img src={item.photo} alt="" />
                      </Col>
                      <Col span={16} className="tw-px-2">
                        <div className="tw-font-semibold">{item.name}</div>
                        <div style={{ fontSize: "10px" }} className="tw-text-gray-400">{item.description}</div>
                      </Col>
                      <Col span={4}>
                        <div className="tw-text-right tw-font-semibold">
                          <CurrencyFormat
                            value={item.price}
                            displayType={"text"}
                            thousandSeparator={true}
                          />
                        </div>
                      </Col>
                    </Row>
                  </>
                ))}
              </>
            ))}
          </div>


          <div className="tw-sticky tw-bottom-0 tw-z-40 tw-bg-white tw-w-full tw-items-center tw-block sm:tw-hidden" style={{ boxShadow: "rgb(100 100 111 / 20%) 0px 7px 29px 0px;" }}>
            <Row className="tw-p-2">
              <Col span={12}>
                <div className="tw-text-center"
                  onClick={() => {
                    router.push("/");
                  }}
                >
                  <img src="assets/home2.png" width="12%" className="tw-m-auto" />
                  <div className="tw-text-gray-400 tw-text-xs">Home</div>
                </div>
              </Col>
              <Col span={12}>
                <div className="tw-text-center"
                  onClick={() => {
                    router.push("/menu");
                  }}
                >
                  <img src="assets/menu1.png" width="12%" className="tw-m-auto" />
                  <div className="tw-text-xs">Menu</div>
                </div>
              </Col>
            </Row>
          </div>
        </>
      </div>
    );
  }
}

const mapState = (state) => ({
  menu: state.menu.payload,
});
const mapDispatch = {
  getMenu: menuAction.browse,
};

export default withRouter(connect(mapState, mapDispatch)(Menu));
