import { faEnvelope } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Image } from "antd";
import { Component } from "react";

class IconGroup extends Component {
  render() {
    const {
      icon = "/icon/box.svg",
      label = "Label",
      value = "value",
    } = this.props;

    return (
      <>
        <div className="tw-flex tw-flex-wrap tw-items-center">
          <div className="tw-h-10 tw-w-10 tw-flex-none tw-text-center">
            <Image
              src={icon}
              className="tw-text-white"
              height="18"
              width="18"
              preview={false}
            ></Image>
          </div>
          <div className="tw-flex-auto tw-pl-2">
            <div className="tw-text-sm tw-text-gray-400 tw-font-medium">
              {label}
            </div>
            <div>{value}</div>
          </div>
        </div>
      </>
    );
  }
}

export default IconGroup;
