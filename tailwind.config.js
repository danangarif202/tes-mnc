const colors = require("tailwindcss/colors");

module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  prefix: "tw-",
  theme: {
    extend: {
      colors: {
        cyan: colors.cyan,
        amber: colors.amber,
        primary: {
          50: "#FCF6EE",
          500: "#AA8F00",
          700: "#da8903",
        },
        cream: {
          300: "#F6EFC7",
          400: "#FFF9DE",
          500: "#d0c17a",
        },
        redbold: {
          500: "#C9302C"
        }
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
